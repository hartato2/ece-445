# ECE 445



# Jan 31th, 2022 Week

This week’s objective was to get an idea that we can make into a proposal. 	

During this week, we brainstormed on ideas on what our project should be. In the end, we came up with the idea of a vending machine that is used to hold medical supplies that will be accessible to students throughout campus. We got the idea from our past experience where we wanted medical supplied, but they are not readily available. 

Implement it with a new feature, which is a failsafe system and a backend. Failsafe system uses a line sensor a backend is used to check for eligibility – to follow McKinley protocols. 
Decided to talk to Professor Schuh about it and he told us that the idea is great, but we are not allowed to dispense specific medicines, it should only be over the counter. Should also make sure that we are not going to violate any FERPA rule, since we cannot get access to anyone’s medical record. 

Because of this, we cannot link our product with McKinley. We have to make our own database and check on it on our own instead of having our system connect to the already existing McKinley database. We have revised our product to make it dispense simple medical supplies such as Advils, masks, and other over the counter products. 
With this, I personally focused on trying to get the general system and idea to work. I tried to research on the available microcontroller that we can use that are Wi-Fi capable. We found TI CC3220F and TI430 microcontroller that can be used with our project. 

[link](https://www.ti.com/lit/ds/symlink/cc3220sf.pdf?ts=1645642439743&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FCC3220SF)

[link](https://www.ti.com/lit/ds/symlink/cc430f5137.pdf?ts=1645642365114&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FCC430F5137)

We chose CC3220SF model because it has enough pins for our design compared to TI 430 
# February 7th, 2022 Week

This week’s objective was to make sure that our product will work and to research on how to create our product. 

After talking to the machine shop, we decided that we had to scale down our design. We first wanted to make it modular so that it will work with multiple trays and that we can scale it up or down. However, after talking to the machine shop, they said that they can only make 1 or 2 trays, so our final product will only be a proof of concept. In addition, the product will be made out of wood, so we do not have a reflective surface. 

In addition, I started to look for parts that can be used for our project and find whether or not they are compatible with each other. Me and Josh looked over different products and tried to make sure that the microcontroller will have enough pins so that it can work. Me and Josh came up with different items that can be used. I found a combination of devices that will use a total of 28 pins, but Josh found one that will most likely only need 25 pins. 

Link to items can be found in the design document. 

![](image_67140353.JPG)

# February 14th, 2022 Week

This week’s objective was to design the logic of the entire system. We had to think of how our product is going to be designed and how each part is going to communicate with each other. 
Initially, we discussed the problem together as a group and came to the conclusion that there are going to be 3 main design – The logic components in the main PCB, the logic for the tray PCB and the entire software side of the project. 

I decided to take upon the logic that is going to be in the tray PCB. After discussing with Josh, we decided that the logic should work. The logic consists of generally two logic gates, a XOR gate and a AND gate. This will be used to identify whether the motor should turn on or not. The XOR gates are going to take in the signal from the BUS, and this will be ANDed together to see whether the motor should be turned on or not. The reason for the XOR gate is to compare each of the 4 bits to see if they match. If they do not match, then it will be a 1 signal. If they match, it will be a 0 signal. All the output from the XOR gates will go through a NOT, then it will be sent to the 4-input AND gate. If all the signals matches, then the input to the AND gate will all be 1, which will enable the motor. If any of the signal does not match, it will send a 0 to the motor, hence not turning on the motor. 

![](image_67134977.JPG)

# February 21st, 2022 Week

This week was generally taken up to complete the design document. We worked on it together and went to get a review on it and made updates accordingly. 

# February 28th, 2022 Week

In this week, my job was to get the PCB design completed and approved by the end of this week. 

At the start, I was in charge of designing the PCB and connecting all the components together with the traces on KiCAD. After doing it, we went to design review and they said that the trace was too close together, so I had to redesign the PCB to make sure that the connections are not too close too each other. A method that we thought of was to split it further into 2 PCBs, but after some redesign, we did not have to. 

# March 7th, 2022 Week

In this week, we must submit the PCB design and our goal is to order the parts that we need before spring break, so that after break we can start on the PCB soldering. 
While I am making some purchases, I noticed that our microcontroller unit that we designed our PCB for ran out of stock. I tried looking all around different vendors and they all ran out of stock and the lead time for the microcontroller is 70 weeks. I tried looking for vendors in other countries as well and they all ran out of stock. Because of this, I had to go researching for more microcontrollers. My initial plan is to buy a development board that contains that microcontroller and try to use a heat gun to desolder it. However, there runs the risk, of breaking the microcontroller under the heat, and we did not want to risk that.

Our TA suggested us to use a different microcontroller. We had to use a new microcontroller in our design. I made the call to the microcontroller that our TA suggested. It was an ESP32 microcontroller. It was compatible with both Bluetooth and Wi-Fi, and it had quite a lot of GPIO pins that we can use. However with this, we had to redesign our PCB to accommodate for the new microcontroller. As a result, this generally took up the rest of the week. 

[link](https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf)

# March 21st, 2022 Week

For this week, our goal was to finish soldering everything unto the PCB and check the connections and functionality of it. We wanted to make sure that the connections are all proper. 
After getting our parts ordered in, we checked our PCB and noticed that I used the wrong footprints for the PCB. There were no holes on the footprint for us to connect our devices. We used the wrong connector footprint in KiCAD. As a result, we do not have a PCB for us to work on because soldering the gates and microcontroller unto the PCB was of no use, as it cannot connect to anything else. As a result, I had to change up the PCB design a little bit and put an order in for the second round of PCB. 

Because of this, we were unable to meet our goal. Me and Matthew decided to work on the development board that we bought and try to make the other components of our PCB work with the microcontroller. At first, we tried to initialize the software and connect the microcontroller with the IDE. After working on it for 2 days, we were unable to find out why the microcontroller would not connect with our IDE. At one point, I noticed that the microcontroller was not even connected properly to the computer, and that was because the driver for the COM port was not updated. After updating it, the microcontroller is connected, and it can now be flashed. 

Me and Matthew started working on the RFID. We tried to connect the RFID with the microcontroller, but it was not connecting. The microcontroller kept on displaying that the RFID reader is not detected. After reading multiple forums and posts, people have said that it was because of the baud rate of the microcontroller and also because of the poll rate of the RFID reader. We tried modifying these and it solved the problem. 

[link](https://github.com/adafruit/Adafruit-PN532/issues/80#issuecomment-649915457)

# March 28th, 2022 Week

Because we are still waiting for a new PCB, the goal for this week is to make all the other component work with the development board. Specifically, we wanted to make sure that the RFID, LCD, and motor works with the microcontroller. 

During this week, I tried to run multiple tests with the RFID reader and found that the RFID reader works with most cards, but it does not work with I-Cards. All other cards that I used sends a consistent data ID for the card, but the I-Card always has the data shuffled around. We concluded that the I-card has some encryption protocol in them, which is why it always has an inconsistent ID. Other than that, the RFID module is working properly and is communicating with the microcontroller. 

I also worked on the motor and tried to run some tests. The motor that we bought is supposed to work with 12 Volts, but after running some tests, the motor works with 5V, but it is running at a much lower speed. At this point, I realized that the wrong motor is purchased, as it only has 2 pins – power and ground. I thought that the motor will have 4 pins, 1 for power, 1 for ground, and then 2 pins for disabling and enabling the motor. 

# April 4th, 2022 Week

During this week, the goal is still to try and make the LCD and motor work with the microcontroller. In addition to this, I was also working on the power subsystem. 
I tried to work on the motor and the microcontroller. Because the wrong motor was purchased, I ended up changing the design and end up connecting the power pin for the motor straight to the output pin of the logic. At this time, since there is no PCB, I decided to connect it straight to the microcontroller and tried to turn on the motor. The motor did not turn on and it turns out that it was because the voltage that is being sent from the microcontroller was 3.3V. I probed the connections and it was working as intended, but it just did not have enough voltage. The solution to this is to use a 5V source. 

I was also working on the power subsystem. I had to wire the wall power to the transformer and try to check if the correct voltage is being output. After adjusting the wires and connections that are connected to the transformer, I used a multimeter to check the voltage and see if it is working as intended. It outputs 5V as intended and it is working correctly. From there, I also soldered connections from the 5V output and connected it to the BUS port for the tray PCB and to the main PCB. 

# April 11th, 2022 Week

During this week, the goal is to try and solder everything unto the new PCB that came in and try to make sure that the connections work. With this, we are also trying to finally test everything so that all the components work together with our PCB. 

During this week, the second round order of PCB came in and I was in charge of soldering all of the components. I first tried to solder the tray PCB and try to get that finished first, then move unto the main PCB. After I finished the tray PCB, the problem that came up was that the logic was not behaving properly. It is sent signal from a power source and it was probed with a multimeter but the voltage output is not behaving as expected. This was later on fixed later in the week because the connection was not soldered properly. 

The main PCB is then soldered but when the PCB tries to be flashed, it is not working. After debugging this issue and reading and the manual and datasheet, it is found that one of the pins required for flashing is connected wrong in our PCB. To flash the microcontroller, it needs to go from a High to Low, then to a High again. However, our PCB connects that pin to an active high, so the microcontroller cannot be flashed. As a result, we decided to resort to a breadboard. We decided to basically copy over our design and logic from the PCB unto the breadboard, and use the development board instead of our PCB. 

![](Picture1.jpg)

We also tried to connect all the other components that we have together, and we have successfully connected the microcontroller with the RFID, LCD, and the database. The only parts that are not finished now is the main PCB, which will have to be translated unto a breadboard
 
# April 18th, 2022 Week

During this week, the goal is to complete the product, that is to make everything work together. 

During this week, we basically all worked together to try and piece all our parts together and debug all the problems that we have together. After the breadboard implementation is complete, we put together all the parts and connect them together. 

When we run the program, the motor does not run for some reason. Because of this, we have to backtrack all of our logic together. First, we probe the microcontroller to see if there is correct data being sent. After backtracking all of the output pins from the entire product, we found that the problem lies in the BUS connections. The BUS pins output a voltage of 3.3V from the ESP32 microcontroller. Since the signal being sent over is 3.3V, but the logic gates operate in 5V, the logic gate interpret the 3.3V as a low; it is saying that he signal from is a low even though it should be a high. 

The solution to this problem is to use a double inverter. Because we do not have a voltage stepper to step it up from 3.3V to 5V, we decided to use two NOT gates that are powered by 5V to step it up. The logic is to invert the signal twice, and since the gate is powered by 5V, the output from the gates will still be the same signal, but instead of 3.3V it will not be 5V. Another problem that we noticed was that a 0 signal needs to be grounded – it cannot be left open as the gate will think of it as floating. This was a problem that we found as we were probing all of the signals. 
After fixing this issue, our overalls product was working as intended and we almost complete our product. The only part that we had to finish is the failsafe detection system and integrating it with the software. 

![](image_67110657.JPG)

During our Mock Demo, our product for some reason does not work with our TA. We came back to ECEB and found that the XOR gate fried. This was a simple fix and swapping out the gate made the logic work as intended again. The gate was fried because at one point, the power was not grounded properly and it shorted. 

# April 23rd, 2022

In this week, the goal is to make sure that the failsafe detection system works. Me and Matthew tried to find a reason as to why the failsafe detection won’t stop the motor. Since the signal being sent to the system is correct, we concluded that the problem lies in the software. After trying out different methods, we realized that the program is not reading the data properly as the signal is slow. By the time the microcontroller reads the signal, it already continues to the other part of the code and turns the motor on again. Because of this, we restructured one small part and made it check for the signal at ever millisecond instead of after every cycle. With this, we made the product work as intended and we have concluded our project. 


